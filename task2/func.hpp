#ifndef FUNC_H
#define FUNC_H
#include<string>
#include<vector>
using namespace std;
vector<string> findWord(string text);
string removeWords(string text, vector<string> words);
size_t findPos(string text, string word);


#endif