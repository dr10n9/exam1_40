#include "freelancer.hpp"

Freelancer::Freelancer(string name, int age, string progLang, double salary){
	setName(name);
	setAge(age);
	setProgLang(progLang);
	setSalary(salary);
}

void Freelancer::setAge(int age){
	this->age = age;
}

void Freelancer::setName(string name){
	this->name = name;
}

void Freelancer::setProgLang(string progLang){
	this->progLang = progLang;
}

void Freelancer::setSalary(double salary){
	this->salary = salary;
}

int Freelancer::getAge(){
	return this->age;
}

string Freelancer::getName(){
	return this->name;
}

string Freelancer::getProgLang(){
	return this->progLang;
}

double Freelancer::getSalary(){
	return this->salary;
}