#ifndef STORAGE_HPP
#define STORAGE_HPP
#include <vector>
#include "freelancer.hpp"
#include "args.hpp"

void saveToJson(const char * filename, vector<Freelancer *> fls);
vector<Freelancer*> loadFromJson(const char * filename, vector<Freelancer *> fls);

#endif