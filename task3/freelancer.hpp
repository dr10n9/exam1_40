#ifndef FREELANCER_HPP
#define FREELANCER_HPP
#include <string>
using namespace std;
class Freelancer{
	private:
	string name;
	int age;
	string progLang;
	double salary;

	public:
	Freelancer(string name, int age, string progLang, double salary);	
	string getName();
	string getProgLang();
	int getAge();
	double getSalary();
	void setName(string name);
	void setProgLang(string progLang);
	void setAge(int age);
	void setSalary(double salary);
};

#endif