#include "storage.hpp"
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <string.h>
#include <stdlib.h>
#include <jansson.h>
#include <fstream>
#include <iostream>

void saveToJson(const char * filename, vector<Freelancer *> fls){
	
	json_t * json = json_object();
	vector <Freelancer*> tmp ;
	ifstream out(filename);
	if(!out.eof()){
		tmp = loadFromJson(filename, tmp);
		for(int i = 0; i<fls.size(); i++){
			tmp.push_back(fls.at(i));
		}
	}
	out.close();
	char  jsonText[10000] = "[\n\0";
	for(int i = 0; i<tmp.size(); i++){
		Freelancer * fl = tmp.at(i);
		const char * name = fl->getName().c_str();
		const char * progLang = fl->getProgLang().c_str();
		json_object_set_new(json, "name", json_string(name));
		json_object_set_new(json, "age", json_integer(fl->getAge()));
		json_object_set_new(json, "progLang", json_string(progLang));
		json_object_set_new(json, "salary", json_real(fl->getSalary()));
		char * jsonString = json_dumps(json, JSON_INDENT(4));
		strcat(jsonText, jsonString);
		strcat(jsonText, ",\n");
		free(jsonString);		
	}
		jsonText[strlen(jsonText) - 2] = ' ';
        strcat(jsonText, "]\n");
		FILE * out1 = fopen(filename, "w");
		
		fprintf(out1, "%s\n", jsonText);
			
		json_decref(json);
		fclose(out1);

	//fclose(file);
}

vector<Freelancer*> loadFromJson(const char * filename, vector<Freelancer * > fls){
	char jsonStr[1000] = "";
	
	char line[100];
	//string tmp;
	FILE * in = fopen(filename, "r");
    while(fgets(line, 100, in)) {
        strcat(jsonStr, line);
    }
        fclose(in);
		cout << jsonStr;

		json_error_t err;
		json_t * jsonArr = json_loads(jsonStr, 0, &err);
        int index = 0;
        json_t * value = NULL;
		json_array_foreach(jsonArr, index, value){
			Freelancer * fl = new Freelancer("", 0, "", 0.0);
			fl->setName(json_string_value(json_object_get(value, "name")));
			fl->setAge(json_integer_value(json_object_get(value, "age")));
			fl->setProgLang(json_string_value(json_object_get(value, "progLang")));
			fl->setSalary(json_real_value(json_object_get(value, "salary")));
			fls.push_back(fl);
		}
		index++;
		json_decref(jsonArr);
		return fls;
	
}